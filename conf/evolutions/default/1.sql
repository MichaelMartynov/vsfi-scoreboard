# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table additional_score (
  id                        integer auto_increment not null,
  score                     integer,
  description               varchar(255),
  solved_task_id            integer,
  constraint pk_additional_score primary key (id))
;

create table solved_task (
  id                        integer auto_increment not null,
  team_id                   bigint,
  task_id                   integer,
  constraint pk_solved_task primary key (id))
;

create table task (
  id                        integer auto_increment not null,
  title                     varchar(255),
  text                      varchar(4096),
  score                     integer,
  constraint uq_task_title unique (title),
  constraint pk_task primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  username                  varchar(255) not null,
  password_hash             varchar(255) not null,
  is_admin                  tinyint(1) default 0 not null,
  constraint uq_user_username unique (username),
  constraint pk_user primary key (id))
;

alter table additional_score add constraint fk_additional_score_solvedTask_1 foreign key (solved_task_id) references solved_task (id) on delete restrict on update restrict;
create index ix_additional_score_solvedTask_1 on additional_score (solved_task_id);
alter table solved_task add constraint fk_solved_task_team_2 foreign key (team_id) references user (id) on delete restrict on update restrict;
create index ix_solved_task_team_2 on solved_task (team_id);
alter table solved_task add constraint fk_solved_task_task_3 foreign key (task_id) references task (id) on delete restrict on update restrict;
create index ix_solved_task_task_3 on solved_task (task_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table additional_score;

drop table solved_task;

drop table task;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

