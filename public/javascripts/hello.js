var app = angular.module('vsfi-terminal', ['vtortola.ng-terminal', 'vsfi.command.tools', 'vsfi.command.impl']);
app.controller('console', ['$scope', '$rootScope', 'commandBroker',
    function ($scope, $rootScope, commandBroker) {
        setTimeout(function () {
            $scope.$broadcast('terminal-output', {
                output: true,
                text: ['',
                    '____    ____   _______. _______  __  ',
                    '\\   \\  /   /  /       ||   ____||  | ',
                    ' \\   \\/   /  |   (----`|  |__   |  | ',
                    '  \\      /    \\   \\    |   __|  |  | ',
                    '   \\    / .----)   |   |  |     |  | ',
                    '    \\__/  |_______/    |__|     |__| ',
                    '                                     ',
                    "Please type 'help' to open a list of commands"],
                breakLine: true
            });

            $scope.$apply();
            $scope.$broadcast('terminal-command', {
                command: 'change-prompt',
                prompt: {user: $scope.username, path: "vsfi"}
            });

        }, 100);

        $scope.session = {
            commands: [],
            output: [],
            $scope: $scope
        };
        $scope.$watchCollection(function () {
            return $scope.session.commands;
        }, function (n) {
            for (var i = 0; i < n.length; i++) {
                $scope.$broadcast('terminal-command', n[i]);
            }
            $scope.session.commands.splice(0, $scope.session.commands.length);
            $scope.$$phase || $scope.$apply();
        });

        $scope.$watchCollection(function () {
            return $scope.session.output;
        }, function (n) {
            for (var i = 0; i < n.length; i++) {
                $scope.$broadcast('terminal-output', n[i]);
            }
            $scope.session.output.splice(0, $scope.session.output.length);
            $scope.$$phase || $scope.$apply();
        });

        $scope.$on('terminal-input', function (e, consoleInput) {
            var cmd = consoleInput[0];
            try {
                if ($scope.session.context) {
                    $scope.session.context.execute($scope.session, cmd.command);
                }
                else {
                    commandBroker.execute($scope.session, cmd.command);
                }
            } catch (err) {
                $scope.session.output.push({output: true, breakLine: true, text: [err.message]});
            }
        });
    }]);

app.config(['terminalConfigurationProvider', function (terminalConfigurationProvider) {
    terminalConfigurationProvider.config().outputDelay = 15;
    terminalConfigurationProvider.config().allowTypingWriteDisplaying = false;
}])

;
