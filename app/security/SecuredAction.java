package security;

import com.avaje.ebean.Ebean;
import models.User;
import play.Routes;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

/**
 * Created by Alex on 18.06.2015.
 */
public class SecuredAction extends Action.Simple {
    @Override
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {
        String username = ctx.session().get("username");
        if (username!=null&& Ebean.createQuery(User.class).where().eq("username",username).findUnique()!=null){
            ctx.request().setUsername(username);
            return delegate.call(ctx);
        }
        else{
            return F.Promise.pure(Results.redirect("/login"));
        }
    }
}
