package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import models.AdditionalScore;
import models.SolvedTask;
import models.Task;
import models.User;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Content;
import security.AdminAction;
import views.html.admin.main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Alex on 11.06.2015.
 */
@With(AdminAction.class)
public class Admin extends Controller {
    public Result index(){
        return ok((Content) main.render());
    }

    public Result users() {
        ArrayNode users = Json.newArray().addAll(Ebean.createQuery(User.class).findList()
                .stream().map(user -> {user.passwordHash="****";return user;}).map(Json::toJson).collect(Collectors.toList()));
        return ok(users,"UTF-8");
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result createUser(){
        JsonNode newUser = request().body().asJson();
        if (newUser.has("username")&&newUser.has("password")) {
            User.createAdminUser(newUser.get("username").asText(), newUser.get("password").asText());
            return ok();
        }
        else{
            return badRequest();
        }
    }

    public Result removeUser(Integer id){
        User user = Ebean.createQuery(User.class).setId(id).findUnique();
        if (user!=null&&!user.username.equalsIgnoreCase("root")){
            user.delete();
            return ok();
        }
        else{
            return badRequest();
        }
    }

    public Result tasks(){
        ArrayNode tasks = Json.newArray().addAll(Ebean.createQuery(Task.class).findList()
                .stream().map(Json::toJson).collect(Collectors.toList()));
        return ok(tasks);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result createTask(){
        JsonNode taskNode = request().body().asJson();
        if (taskNode.has("title")&&taskNode.has("text")&&taskNode.has("score")){
            Task task = new Task();
            task.score = taskNode.get("score").asInt();
            task.text  = taskNode.get("text").asText();
            task.title = taskNode.get("title").asText();
            task.save();
            return ok();
        }
        else{
            return badRequest();
        }
    }

    public Result removeTask(Integer id){
        Task task = Ebean.createQuery(Task.class).setId(id).findUnique();
        if (task!=null){
            task.delete();
            return ok();
        }
        else{
            return badRequest();
        }
    }

    public Result teams(){
        ArrayNode teams = Json.newArray().addAll(Ebean.createQuery(User.class).where().eq("isAdmin",false).findList()
                .stream().map(Json::toJson).collect(Collectors.toList()));
        return ok(teams);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result createTeam(){
        JsonNode teamNode = request().body().asJson();
        if (teamNode.has("teamname")&&teamNode.has("password")){
            User.createTeamUser(teamNode.get("teamname").asText(),teamNode.get("password").asText());
            return ok();
        }
        else{
            return badRequest();
        }
    }

    public Result getSolvedTasks(){
        List<SolvedTask> allSolved = Ebean.find(SolvedTask.class).findList();
        List<JsonNode> solvedTasks = allSolved.stream().map(solvedTask -> SolvedJson.createObject(solvedTask.id, solvedTask.team.username, solvedTask.task.title))
                .map(Json::toJson).collect(Collectors.toList());
        return ok(Json.newArray().addAll(solvedTasks));
    }

    public Result deleteSolved(Integer id){
        SolvedTask task = Ebean.find(SolvedTask.class).setId(id).findUnique();
        if (task!=null) {
            task.delete();
            return ok();
        }
        else{
            return badRequest();
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result solveTask(){
        JsonNode solveNode = request().body().asJson();
        Integer userId = solveNode.get("team").asInt();
        Integer taskId = solveNode.get("task").asInt();
        User team = Ebean.find(User.class).setId(userId).findUnique();
        Task task = Ebean.find(Task.class).setId(taskId).findUnique();
        if (Ebean.find(SolvedTask.class).where().eq("team",team).eq("task",task).findUnique()==null){
            List<AdditionalScore> scoreList = new ArrayList<>();
            JsonNode adObjects = solveNode.get("adObjects");
            if (adObjects!=null&&adObjects.isArray()){
                ArrayNode node = (ArrayNode) adObjects;
                for (JsonNode adObject:node){
                    scoreList.add(new AdditionalScore(adObject.get("score").asInt(),adObject.get("description").asText()));
                }
            }

            AdditionalScore[] scores = new AdditionalScore[scoreList.size()];
            SolvedTask.solveTask(team,task, scoreList.toArray(scores));
            return ok();
        }
        else {
            return badRequest();
        }
    }

    private static class SolvedJson{
        public Integer id;
        public String username;
        public String task;

        public static SolvedJson createObject(Integer id, String username, String task){
            SolvedJson json = new SolvedJson();
            json.id = id;
            json.username = username;
            json.task = task;
            return json;
        }
    }
}
