package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import models.*;
import org.mindrot.jbcrypt.BCrypt;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Content;
import security.SecuredAction;
import views.html.index;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@With(SecuredAction.class)
public class Application extends Controller {
    public Result index() {
        return ok((Content) index.render(request().username()));
    }

    public Result scoreboard(){
        String scoresQuery = "SELECT t_user.username as team, SUM(t_task.score) as score " +
                "FROM solved_task,user as t_user,task as t_task WHERE " +
                "solved_task.team_id = t_user.id and solved_task.task_id=t_task.id GROUP BY team";

        List<SqlRow> scoreRows = Ebean.createSqlQuery(scoresQuery).findList();

        Map<String,Integer> scores =
                scoreRows.stream().collect(Collectors.toMap(
                        sqlRow -> sqlRow.getString("team"),
                        row -> row.getInteger("score")));

        String adScoreQuery = "SELECT t_user.username as team, SUM(additional_score.score) as adScore " +
                "FROM solved_task, additional_score, user as t_user WHERE solved_task.id=additional_score.solved_task_id " +
                "AND solved_task.team_id=t_user.id GROUP BY team;";
        List<SqlRow> adScoreRows = Ebean.createSqlQuery(adScoreQuery).findList();

        Map<String,Integer> adScore =
                adScoreRows.stream().collect(Collectors.toMap(
                        sqlRow -> sqlRow.getString("team"),
                        sqlRow -> sqlRow.getInteger("adScore")));

        List<SimpleScoreboard> list = new ArrayList<>();

        for (String team:scores.keySet()){
            SimpleScoreboard simpleScoreboard = new SimpleScoreboard();
            simpleScoreboard.team = team;
            simpleScoreboard.total = scores.get(team);
            if (adScore.containsKey(team)){
                simpleScoreboard.total+=adScore.get(team);
            }
            list.add(simpleScoreboard);
        }
        list.sort((o1, o2) -> Integer.compare(o1.total,o2.total));

        return ok(Json.toJson(list));
    }

    public Result teamScoreboard(String teamname){
        List<SolvedTask> tasks = Ebean.createQuery(SolvedTask.class).where().eq("team",
                Ebean.createQuery(User.class).where().eq("username",teamname).findUnique()).findList();
        Map<String,F.Tuple<Integer,List<F.Tuple<String,Integer>>>> extendedJson = tasks.stream().collect(Collectors.toMap(
                (t) -> t.task.title,
                (t) -> F.Tuple(t.task.score, t.scoreList.stream().map(
                        (s) -> F.Tuple(s.description, s.score)).collect(Collectors.toList()))));

        return ok(Json.toJson(extendedJson));
    }

    public Result task(){
        return ok(Json.toJson(Ebean.find(Task.class).findList().stream().collect(Collectors.toMap((t)->t.id,(t)->t.title))));
    }

    public Result singleTask(Integer id){
        Task task = Ebean.createQuery(Task.class).setId(id).findUnique();
        return ok(Json.toJson(task));
    }


    private class SimpleScoreboard{
        public String team;
        public int total;
    }
}
