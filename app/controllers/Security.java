package controllers;

import com.avaje.ebean.Ebean;
import exception.WrongPasswordException;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Content;
import security.SecuredAction;

/**
 * Created by Alex on 18.06.2015.
 */

public class Security extends Controller {
    public Result login(){
        return ok((Content) views.html.login.render(Form.form(UserFields.class)));
    }

    public Result doLogin(){
        Form<UserFields> form = Form.form(UserFields.class).bindFromRequest(request());
        if (form.hasErrors()){
            return ok((Content) views.html.login.render(form));
        }
        else{
            User currentUser = Ebean.createQuery(User.class).where().eq("username",form.get().username).findUnique();
            if (currentUser!=null){
                session().put("username",currentUser.username);
            }
            return redirect("/");
        }
    }

    @With(SecuredAction.class)
    public Result logout(){
        session().clear();
        return temporaryRedirect("/");
    }

    public static class UserFields {
        @Constraints.Required
        public String username;
        @Constraints.Required
        public String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String validate(){
            try {
                if (User.findUserByUsernameAndPassword(username,password)==null){
                    return "Пользователь не существует";
                }
                else{
                    return null;
                }
            } catch (WrongPasswordException e) {
                return "Неверный пароль";
            }
        }
    }
}
