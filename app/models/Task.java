package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Alex on 18.06.2015.
 */
@Entity
public class Task extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(unique = true)
    @Constraints.MaxLength(value = 255)
    public String title;

    @Constraints.MaxLength(value = 8096)
    @Column(length = 4096)
    public String text;

    public Integer score;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "task", fetch = FetchType.LAZY)
    public List<SolvedTask> solvedTasks;

    public static Task createTask(String title, String text, Integer score){
        Task task = new Task();
        task.title = title;
        task.text = text;
        task.score = score;
        task.save();
        return task;
    }
}
