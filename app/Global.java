import com.avaje.ebean.Ebean;
import controllers.Admin;
import models.AdditionalScore;
import models.SolvedTask;
import models.Task;
import models.User;
import play.*;

public class Global extends GlobalSettings {

    public void onStart(Application app) {
        if(Ebean.createQuery(User.class).where().eq("username","root").findUnique()==null){
            User.createAdminUser("root", Play.application().configuration().getString("root.password"));
        }
        Logger.info("Application has started");
    }

    public void onStop(Application app) {
        Logger.info("Application shutdown...");
    }

}