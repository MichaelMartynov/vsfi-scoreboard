name := """vsfi-scoreboard"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean,SbtWeb)
scalaVersion := "2.11.6"
pipelineStages := Seq(gzip)

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)

libraryDependencies += "org.mindrot" % "jbcrypt" % "0.3m"
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.35"



// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

